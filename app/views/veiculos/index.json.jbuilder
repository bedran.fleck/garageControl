json.array!(@veiculos) do |veiculo|
  json.extract! veiculo, :id, :modelo, :placa, :CPF, :nome_proprietario
  json.url veiculo_url(veiculo, format: :json)
end
