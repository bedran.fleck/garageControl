class VeiculosController < ApplicationController
	before_action :find_veiculo, only: [:show, :edit, :update, :destroy]

	def index
		@veiculos = Veiculo.all.order("created_at DESC")
	end

	def show
		@veiculo = Veiculo.find(params[:id])
	end

	def new
		@veiculo = Veiculo.new
	end

	def create
		@veiculo = Veiculo.new(veiculo_params)
	
		if @veiculo.save
			redirect_to root_path
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @veiculo.update(veiculo_params)
			redirect_to veiculo_path(@veiculo)
		else
			render 'edit'
		end
	end

	def destroy
		@veiculo.destroy
		redirect_to root_path
	end


	private

		def veiculo_params
			params.require(:veiculo).permit(:modelo, :placa, :rg_proprietario, :proprietario)
		end	

	def find_veiculo
		@veiculo = Veiculo.find(params[:id])
	end

end
