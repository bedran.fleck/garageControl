class CreateVeiculos < ActiveRecord::Migration
  def change
    create_table :veiculos do |t|
      t.string :modelo
      t.string :placa
      t.string :rg_proprietario
      t.string :proprietario

      t.timestamps null: false
    end
  end
end
